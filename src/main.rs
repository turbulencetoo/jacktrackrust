#![feature(nll)]

// #[macro_use]
// extern crate serde_derive;

// extern crate serde;
// extern crate serde_json;

use std::rc::{Rc, Weak};
use std::cell::RefCell;
use std::collections::{HashMap, HashSet};


// #[derive(Debug, Serialize, Deserialize)]
struct Node {
    idx: String,
    children: Vec<Weak<RefCell<Node>>>,
    parents: Vec<Weak<RefCell<Node>>>
}

impl PartialEq for Node {
    fn eq(&self, other: &Node) -> bool {
        self.idx == other.idx
    }
}
impl Eq for Node {}

impl Node {
    fn new(idx: &str) -> Self {
        Node {idx: idx.to_owned(), children: vec![], parents: vec![]}
    }
}

// #[derive(Serialize, Deserialize)]
struct JackTree {
    turns: HashMap<u32, Vec<Rc<RefCell<Node>>>>,
    cur_det_locs: Vec<String>,
    cur_level: u32  // this is an int where in python i had it as a str, TODO should node inx be int too?
}

impl JackTree {
    fn new(start_node_idxs: &[&str]) -> Self {
        let roots = start_node_idxs
                    .iter()
                    .map(|i| Rc::new(RefCell::new(Node::new(i))))
                    .collect::<Vec<_>>();
        let mut turns = HashMap::new();
        turns.insert(0, roots);

        JackTree{ turns: turns, cur_det_locs: vec![], cur_level: 0}
    }

    fn add_node(&mut self, idx: &str, parent: &Rc<RefCell<Node>>, merge: bool) {
        // Stolen exactly from the python code
        let cur_nodes = self.turns.get_mut(&self.cur_level).unwrap();
        let new_node: Rc<RefCell<Node>>;
        if merge {
            if let Some(node) = cur_nodes.iter().find(|n| n.borrow().idx == idx) {
                new_node = Rc::clone(node);
            }
            else {
                new_node = Rc::new(RefCell::new(Node::new(idx)));
                cur_nodes.push(Rc::clone(&new_node));
            }
        }
        else {
            new_node = Rc::new(RefCell::new(Node::new(idx)));
            cur_nodes.push(Rc::clone(&new_node));
        }
        parent.borrow_mut().children.push(Rc::downgrade(&new_node));
        new_node.borrow_mut().parents.push(Rc::downgrade(parent));
    }

    fn next_level(&mut self) {
        self.cur_level +=1 ;
        self.turns.insert(self.cur_level.clone(), vec![]);
    }

    fn find_node(&mut self, idx_to_find: &str) {
        let mut safe_nodes = HashSet::new();
        for node in self.turns.get(&0).unwrap() {
            let mut cur_path = Vec::new();
            find_node_rec(idx_to_find, &Rc::downgrade(&node), &mut cur_path, &mut safe_nodes);
        }
        for (&turn, nodes) in self.turns.iter_mut() {
            // keep only nodes that were marked safe
            nodes.retain(|n| safe_nodes.contains(&(turn, n.borrow().idx.clone())));
        }
        self.street_sweeper();
    }

    fn dont_find_node(&mut self, idx_to_not_find: &str) {
        for (_, nodes) in self.turns.iter_mut() {
            nodes.retain(|n| n.borrow().idx != idx_to_not_find);
        }
        self.street_sweeper();
    }

    fn dont_find_node_missed_arrest(&mut self, idx_to_not_find: &str) {
        let leaves = self.turns.get_mut(&self.cur_level).unwrap();
        leaves.retain(|n| n.borrow().idx != idx_to_not_find);
        self.street_sweeper();
    }

    fn street_sweeper(&self) {
        // Remove all nodes in the tree that have had their strong refs yanked
        for (_, nodes) in self.turns.iter() {
            for node in nodes.iter() {
                node.borrow_mut().children.retain(|c| c.upgrade().is_some());
                node.borrow_mut().parents.retain(|p| p.upgrade().is_some());
            }
        }
        // TODO: orphan clean up? we want all leaves to be at the same level
        // Should start at 2nd to last level and remove all nodes with
        //  no children and work our way up
    }
}

fn find_node_rec(idx_to_find: &str,
                 cur_node: &Weak<RefCell<Node>>,
                 cur_path: &mut Vec<String>,
                 safe_nodes: &mut HashSet<(u32, String)>)
{
    if let Some(node) = cur_node.upgrade() {
        cur_path.push(node.borrow().idx.clone());

        if node.borrow().children.len() == 0 {
            // We've reached the end of the tree
            // If we've seen the node target, mark all nodes in current path as safe
            if cur_path.contains(&idx_to_find.to_owned()) {
                for (turn, idx) in cur_path.iter().enumerate() {
                    safe_nodes.insert((turn as u32, idx.clone()));
                }
            }
        }
        else {
            // In this case children exist so make a recursive call
            for child in node.borrow().children.iter() {
                find_node_rec(idx_to_find, child, cur_path, safe_nodes);
            }
        }
        cur_path.pop();
    }
}

// fn sever_relationship(parent: &Weak<RefCell<Node>>, child: &Weak<RefCell<Node>>){
//     if let Some(parent) = parent.upgrade() {
//         parent.borrow_mut().children.retain(|c| { let c = c.upgrade();
//                                                   c.is_some() && c != child.upgrade()});
//     }
//     if let Some(child) = child.upgrade(){
//         child.borrow_mut().parents.retain(|p| { let p = p.upgrade();
//                                                 p.is_some() && p != parent.upgrade()});
//     }
// }


fn main() {
    println!("Hello, world!");
}
